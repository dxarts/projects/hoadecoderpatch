// main parameters
IdentityDictionary(know: true)
.putPairs([
	'numSatChans',			6, // the number of satellite speakers
	'numSubChans', 			2, //  the number of sub-woofers, if no subs set to 0
	'outbusNums',           [0, 2, 4, 6, 8, 10] ++ [19, 20], //  the actual output channel numbers for each speaker (including subs at the end)
	'decoderType',          'discrete', // decoder type ('modeMatch', 'panto', 'matrix')
	'discreteType',         'hex', // when discrete, which type of input ('quad', 'hex', 'oct', or 'all'), supplied in LRLR... (when 'discrete' and this is 'all', pass through)
	'encodeType',           false, // when discrete ( not 'all' ) enable transcoding via bformat
])