TITLE:: HoaMultiBandFocl
summary:: Design and implement Hoa Multiband Focalization Filters
categories:: Libraries>Ambisonic Toolkit>HOA>Decoding
related:: Classes/HoaDecoder, Classes/HoaMatrixDecoder

DESCRIPTION::
Design multiband focalization filters using link::Classes/Signal#-hoaMultiBandFocl:: and convolve with BFormat signals.


CLASSMETHODS::

METHOD:: new

ARGUMENT:: configPath
A path to the .scd configuration file. See the config folder for examples.

ARGUMENT:: server
An optional instance of Server on which to load the focalization filter buffers.

DISCUSSION::

The following is a list of the possible parameters to set in the config file. Each parameter has a default value if nothing is set. To use the default, either set the parameter to teletype::nil::, or simply ommit the entry from the IdentityDictionary. Please set the link::Classes/IdentityDictionary::'s know argument to teletype::true::.

table::
## satOrder || The ambisonic order of the satellites.
## subOrder || The ambisonic order of the subs.
## psyFoclSize || The fftsize of the kernels.
## alpha || The alpha value. Reduces to rectangular window at zero and narrows at higher values. Parameterized as described here: https://ccrma.stanford.edu/~jos/sasp/Kaiser_Window.html.
## foclRadius || The focalisation radius, in meters. Default is the Atk reference radius.
## mainBeamDict || A dictionary specifying beam shapes and edge frequencies for the satellites. See link::Classes/HoaOrder#Beaming%20&%20Decoder%20matching#Beaming & Decoder Matching::.
## satDim || The number of dimensions for the satellites. Default is 3.
## subDim || The number of dimensions for the subs. Default is 2.
## match || Matching criteria, see link::Classes/HoaOrder#Beaming%20&%20Decoder%20matching#Beaming & Decoder Matching::. Can be 'amp', 'rms', or 'energy'.
## subBeamDict || A dictionary specifying beam shapes for the subs. See link::Classes/HoaOrder#Beaming%20&%20Decoder%20matching#Beaming & Decoder Matching::.
## sampleRate || An optional sample rate for generating the kernels. Set to nil or ommit to use the supplied Server sample rate.
##psyFoclKernelPath || The path where to write the kernels. Default is the link::Classes/Platform#-defaultTempDir::. directory.
::

METHOD:: exampleConfigPath
The path to the example configuraiton files folder.


INSTANCEMETHODS::

SUBSECTION:: UGen Graph and SynthDefs

METHOD:: ar
The UGen graph to run the convolution with the kernel filters. This returns a flat array with the satellite BFormat signal and the sub BFormat signal. For instance if the satellite order is 3 and the sub order is 1, this would be an array of size 20, with the first 16 being the satellites BFormat channels and the last 4 the sub BFormat channels.

ARGUMENT:: in
The BFormat input signal.

METHOD:: synthDef
An instance of CtkSynthDef that takes an input from an audio buss and performs the convolution and sends to an out buss, with arguments teletype::in_busnum, out_busnum, gate, fadeTime::.

SUBSECTION:: Kernel Buffers

METHOD:: kernelBuffers
The Array of CtkBuffers with the kernels.

DISCUSSION::
These will need to be either loaded onto the real-time server or added to a NRT CtkScore or Score.

SUBSECTION:: Configuration Parameters

METHOD:: sampleRate
The sample rate to use when writing the kenrels to disk.

METHOD:: satOrder
The ambisonic order of the satellites.

METHOD:: subOrder
The sub order.

METHOD:: kernelPath
The path to wich to write the kernels.

METHOD:: psyFoclKernelPath
The actual kernel path based on all the supplied parameters.

METHOD:: foclRadius
The focalisation radius.

METHOD:: alpha
The alpha value.

METHOD:: match
The match method.

METHOD:: mainDim
The dimensions of the satellites.

METHOD:: subDim
The sub dimension.

METHOD:: mainBeamDict
The beam dictionary for the satellites.

METHOD:: subBeamDict
The beam Dictionary for the subs.

METHOD:: psyFoclSize
The fft size of the kernels.

METHOD:: config
The IdentityDictionary from the config file.

PRIVATE:: prGenerateFoclKernels, stringToPathName, init, loadAttributes


EXAMPLES::

code::
// focalization kernels only

(
s.waitForBoot({

	~multiBand = HoaMultiBandFocl.new(HoaMultiBandFocl.exampleConfigPath +/+ "FoclConfigExample.scd", s);

	// load the kernel buffers
	~multiBand.kernelBuffers.do{ |buffer| buffer.load };

	s.sync;

	~buffer = CtkBuffer("/Users/dan/Library/Application Support/ATK/sounds/HOA3/Pampin-On_Space_HOA3_Atk.wav", server: s).load;

	s.sync;

	~synth = CtkSynthDef('testHOA', { |buffer, xover|
		var in;
		in = PlayBuf.ar(~buffer.numChannels, buffer, BufRateScale.kr(buffer), loop: 1);
		in = ~multiBand.ar(in);
		Out.ar(0, in)
	});

	s.sync;

	~note = ~synth.note.buffer_(~buffer).play;
})

)

// free the synth
~note.free;

// free the buffers
~multiBand.kernelBuffers.do(_.free)


::