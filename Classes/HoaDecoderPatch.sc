HoaDecoderPatch {
	var patchConfigPath, defaultDecoderName, listeningRadius, server;
	var <decoder, <multiBandFocl, <decoderName;
	var inbusnum, outbusnum, <>stereoActive=false;
	var <decodersynth;
	classvar <exampleConfigPath;

	*initClass{
		exampleConfigPath = File.realpath(HoaDecoderPatch.filenameSymbol.asString.dirname +/+ "../config/")
	}

	*new { |patchConfigPath, defaultDecoderName, satOrder, subOrder, inputFormat, listeningRadius, server|
		^super.newCopyArgs(patchConfigPath, defaultDecoderName, listeningRadius, server).init(satOrder, subOrder, inputFormat)
	}

	init { |satOrder, subOrder, inputFormat|

		server = server ?? { Server.default };

		decoder = HoaDecoder.new(patchConfigPath, defaultDecoderName, satOrder, subOrder, inputFormat, listeningRadius, server);

		decoderName = decoder.decoderName;

		decoder.loadKernels;

	}

	stringToPathName { |path, relativePath = (PathName(File.realpath(HoaDecoder.filenameSymbol)).pathOnly)|
		PathName(path).isAbsolutePath.if({//it's absolute path or standard path
			^PathName.new(path);
		}, { // its relative path
			^PathName.new(relativePath ++ path)
		})

	}

	//convenience methods
	designModeMatcher { |beamShape = 'basic', match = 'energy'|
		decoder.designModeMatcher(beamShape, match)
	}

	designADTAllRad { |filename = 'ADT', beamShape = 'energy', match = 'amp', imagSpeakers, overwrite = false, verbose = false|
		decoder.designADTAllRad(filename, beamShape, match, imagSpeakers, overwrite, verbose)
	}

	designFromMatrix { |filename = 'ADT', beamShape = 'energy', match = 'amp', pathToMatrix|
		decoder.designFromMatrix(filename, beamShape, match, pathToMatrix)
	}

	designSubModeMatcher { |beamShape = 'basic', match = 'energy'|
		decoder.designSubModeMatcher(beamShape, match)
	}

	designSubDirections { |beamShape = 'basic'|
		decoder.designSubDirections(beamShape)
	}

	play { |inbus, outbus, xover, satGain, subGain, xfade = 0.2, addAction = 0, target = 1, stereo=false, amp=1|
		// protect against CtkAudio
		outbus.isKindOf(CtkAudio).if({ outbus = outbus.bus });
		inbus.isKindOf(CtkAudio).if({ inbus = inbus.bus });
		inbusnum = inbus;
		outbusnum = outbus;
		stereoActive = stereo;
		stereoActive.if({ inbus = inbus + 2 });
		decodersynth = this.decoder.decoderSynth.note(addAction: addAction, target: target)
		.in_busnum_(inbus)
		.out_busnum_(outbus)
		.amp_(amp);

		xover = xover ?? { decoder.xover_freq };
		decodersynth.xover_freq_(xover);
		satGain !? { decodersynth.satGain_(satGain) };
		subGain !? { decodersynth.subGain_(subGain) };

		decodersynth.fadeTime_(xfade).play;
	}

	pause {
		decodersynth.pause;
	}

	unPause {
		decodersynth.run;
	}

	free { |xfade, cond|
		{
			xfade !? { decodersynth.fadeTime_(xfade) };
			decodersynth.release;
			decoder.useKernels.if({
				decoder.multiBandFocl.buffersActive.if({
					decoder.multiBandFocl.kernelBuffers.do{ |buffer| buffer.free };
					decoder.multiBandFocl.buffersActive = false
				})
			});
			(xfade * 2).wait;
			cond !? { cond.test_(true).signal }
		}.forkIfNeeded
	}

	routeForStereo { |bool|
		(stereoActive == bool).if({ "No change in Stereo".postln }, {
			stereoActive = bool;
			bool.if({
				"Routed for Stereo!".postln;
				decodersynth.in_busnum_(inbusnum + 2)
			}, {
				"Stereo Cleared!".postln;
				decodersynth.in_busnum_(inbusnum)
			})
		})

	}

	// NOTE: this changes with the stereo setting, so ask the synth
	inbusnum { ^decodersynth.in_busnum }
	outbusnum { ^decodersynth.out_busnum }

	outbusnum_ { |bus| decodersynth.out_busnum_(bus) }
	// TODO: speaker distances and delays may be updated with a new decoder
	// kernelUpdate {
	// 	decodersynth.speakerDistances_(sl.spkrDists);
	// 	compsynth.speakerDelays_(sl.spkrDels).speakerGains_(sl.spkrGains)
	// }
}