+ Array {

	truncateKernel { |threshold = -60.0, onset = true, offset = true, zeroPad = true|
		var onsetIndex = onset.if({  // reverse schroeder integrate
			this.collect({ |item|
				var itemSquared = item.squared;
				((itemSquared.integrate / itemSquared.sum) >= (2 * threshold).dbamp).indexOf(true)
			}).minItem
		}, {
			0
		});
		var offsetIndex = offset.if({  // schroeder integrate (from SignalBox quark)
			this.collect({ |item|
				(item.squared.schroederIntegrate <= (2 * threshold).dbamp).indexOf(true)
			}).maxItem
		}, {
			this.size - 1
		});
		var truncKernel = this.collect({ |item|
			item[onsetIndex .. offsetIndex]
		});

		zeroPad.if({
			var truncSize = offsetIndex - onsetIndex + 1;
			var zeroSize = truncSize.nextPowerOfTwo - truncSize;
			(zeroSize > 0).if({
				truncKernel = truncKernel.collect({ |item|
					item ++ Signal.zeroFill(zeroSize)
				})
			})
		});

		^truncKernel
	}

}
