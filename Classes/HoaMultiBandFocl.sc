HoaMultiBandFocl {
	var config, <satOrder, <subOrder, <numSats, <numSubs, <listeningRadius, server;
	var <>kernelBuffers, <>buffersActive = false;
	var <alpha, <kernelPath, <psyFoclKernelPath, <psyFoclSize, <foclRadius, <mainBeamDict;
	var <mainDim, <match, <sampleRate, <subBeamDict, <subDim;
	var numBChannels, numSubBChannels, listeningRadii, <kernelDelays;
	classvar <exampleConfigPath;

	*initClass{
		exampleConfigPath = File.realpath(HoaDecoder.filenameSymbol.asString.dirname +/+ "../config/")
	}

	*new { |configDict, satOrder, subOrder, numSats, numSubs, listeningRadius, server|
		^super.newCopyArgs(configDict, satOrder, subOrder, numSats, numSubs, listeningRadius, server).init
	}

	init {

		satOrder = satOrder ?? { 3 };

		server = server ?? { Server.default };

		// listening radius by size
		listeningRadii = Dictionary.new;
		listeningRadii['small'] = 0.18 / 2;
		listeningRadii['medium'] = 0.25;
		listeningRadii['large'] = 0.5;

		this.loadAttributes;

		this.prGenerateFoclKernels;

		numBChannels = satOrder.asHoaOrder.size;
		numSubBChannels = subOrder.asHoaOrder.size;

		psyFoclKernelPath.deepFiles.do{ |pathname|
			(pathname.extension == "wav").if({
				kernelBuffers = kernelBuffers.add(
					CtkBuffer(pathname.fullPath, server: server)
				)
			})
		}

	}

	synthDef {

		CtkSynthDef('soundLabHoaConv', { |in_busnum, out_busnum, gate = 1, fadeTime|
			var in, sats, subs, env;

			env = EnvGen.kr(Env([0, 1, 0], [fadeTime, fadeTime], 'sin', 1), gate);

			in = In.ar(in_busnum, numBChannels);

			Out.ar(out_busnum, this.ar(in) * env)

		});

	}

	ar { |in|
		var sats, subs, delay;
		// sats
		sats = kernelBuffers[0..satOrder].collect{ |buffer, i|
			Convolution2.ar(
				in[i.asHoaDegree.indices],
				buffer,
				framesize: psyFoclSize
			)
		}.flatten;

		subs = subBeamDict.notNil.if({
			kernelBuffers[satOrder+1..].collect{ |buffer, i|
				Convolution2.ar(
					in[i.asHoaDegree.indices],
					buffer,
					framesize: psyFoclSize
				)
			}.flatten
		}, {
			// return bformat without applying kernels for subs
			// apply delay here
			// kernelDelay + kernelSize - blockSize
			delay = (kernelDelays[0] + psyFoclSize - ControlRate.ir)/SampleRate.ir;
			DelayN.ar(in[subOrder.asHoaOrder.indices], delay, delay)
		});

		^[sats, subs]

	}

	loadAttributes {
		var atts, freqAtRadius;

		sampleRate = config.sampleRate ?? { server.sampleRate ?? { 48000 } };

		// unpack dec specs
		kernelPath = this.stringToPathName(config.psyFoclKernelPath ?? { Platform.defaultTempDir });

		psyFoclSize = config.psyFoclSize ?? { 1024 };
		// can we assume kernel size is 48000 based, so we can do some math
		psyFoclSize = (psyFoclSize * (sampleRate/48000)).asInteger.nextPowerOfTwo;
		alpha = config.alpha ?? { 1.0 };  // kaiser smoothing window
		foclRadius = config.foclRadius;   // focalisation radius, nil default?
		mainBeamDict = config.mainBeamDict.isNil.if({
			// listening radius and beam dict are nil, set to a default
			listeningRadius.isNil.if({
				IdentityDictionary(know: true)
				.putPairs([
					\beamShapes, [ \basic, \energy ],
					\edgeFreqs, [ 3000.0, 10000.0 ],
				])
			}, {
				// calculate crossovers from listening radius
				freqAtRadius = HoaRadius.new(listeningRadii[listeningRadius]).freq(satOrder);
				IdentityDictionary(know: true)
				.putPairs([
					\beamShapes, [ \basic, \energy ],

					// edge freqs
					\edgeFreqs, [ freqAtRadius, freqAtRadius * 2 ]
				])
			})
		}, {
			// beam dict supplied by config file
			config.mainBeamDict
		});
		mainDim = config.satDim ?? { 3 };
		match = config.match ?? { 'energy' };
		config.subBeamDict.notNil.if({
			subBeamDict = config.subBeamDict;
			subDim = config.subDim ?? { 2 }
		});

		psyFoclKernelPath = kernelPath.fullPath ++ sampleRate.asInteger ++ "/";

		atts = [satOrder, psyFoclSize, alpha, foclRadius, mainBeamDict.beamShapes, mainBeamDict.edgeFreqs, mainDim, match];

		subBeamDict.notNil.if({
			atts = atts ++ [subBeamDict.beamShapes, subDim];
		});

		atts.do{ |param|
			param.isKindOf(Array).if({
				param.do{ |inParam|
					psyFoclKernelPath = psyFoclKernelPath ++ inParam ++ "_"
				}
			}, {

				psyFoclKernelPath = psyFoclKernelPath ++ param ++ "_"
			})
		};

		psyFoclKernelPath = psyFoclKernelPath[0..psyFoclKernelPath.size-2] ++ "/";

		psyFoclKernelPath = PathName(psyFoclKernelPath);

	}

	prGenerateFoclKernels {
		var mainPsychoKernels, subPsychoKernels, kernels;
		// change behavior to always regenerate kernels?
		psyFoclKernelPath.isFolder.not.if({
			psyFoclKernelPath.fullPath.mkdir
		});

		// design kernels
		mainPsychoKernels = Signal.hoaMultiBandFocl(
			size: psyFoclSize,
			radius: foclRadius,
			beamDict: mainBeamDict,
			dim: mainDim,
			match: match,
			order: satOrder,
			sampleRate: sampleRate,
			numChans: numSats,
		);

		mainPsychoKernels = mainPsychoKernels.collect({ |kernel|
			Signal.kaiserWindow(psyFoclSize, a: alpha) * kernel // window
		});

		kernels = mainPsychoKernels;

		subBeamDict.notNil.if({
			subPsychoKernels = Signal.hoaMultiBandFocl(
				size: psyFoclSize,
				radius: foclRadius,
				beamDict: subBeamDict,
				dim: subDim,
				match: match,
				order: subOrder,
				sampleRate: sampleRate,
				numChans: numSubs
			);

			subPsychoKernels = subPsychoKernels.collect({ |kernel| Signal.kaiserWindow(psyFoclSize, a: alpha) * kernel });  // window

			kernels = kernels ++ subPsychoKernels
		});

		// truncate kernels to reduce delay
		kernels = kernels.truncateKernel;

		kernels.do({ |kernel, i|
			kernelDelays = kernelDelays.add(this.schroederPeakIndex(kernel));
			kernel.write(
				path: psyFoclKernelPath.fullPath ++ "FIR_" ++ i.asString.padLeft(2, "0") ++ ".wav",
				headerFormat: "WAV",
				sampleFormat: "float",
				sampleRate: sampleRate
			)
		})  // as a sound file

	}

	stringToPathName { |path, relativePath = (PathName(File.realpath(HoaDecoder.filenameSymbol)).pathOnly)|
		PathName(path).isAbsolutePath.if({//it's absolute path or standard path
			^PathName.new(path);
		}, { // its relative path
			^PathName.new(relativePath ++ path)
		})

	}

	schroederPeakIndex { |signal|
		var signalSq;

		signalSq = signal.squared;

		// schroeder peak index
		^(
			(signalSq.integrate / signalSq.sum) *
			(signalSq.deepCopy.reverse.integrate.reverse / signalSq.sum)
		).maxIndex


	}
}