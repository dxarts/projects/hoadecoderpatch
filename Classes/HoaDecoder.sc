HoaDecoder {
	var configPath, <decoderName, <satOrder, <subOrder, <inputFormat, <listeningRadius, server;
	var patchConfig, decoderConfig;
	var <decoderSynth, orientation;
	var <numBChannels, <numSubChans, <numSatChans;
	var <decoderType, <>beamShape, <>match;
	var <>outbusNums, <satDirections, <subDirections;
	var <>matrix_dec_sat, <>matrix_dec_sub, <numInputChannels;
	var <xover_freq, <useDelays = false, <useKernels = false;
	var <matrixFilePath, <multiBandFocl, <matrix_enc_sat, <matrix_enc_sub, <encodeType, <discreteType;
	var <spDists, <spGains, <spDels, <satOuts, <subOuts, <maxOrder;
	classvar <exampleConfigPath;

	*initClass{
		exampleConfigPath = File.realpath(HoaDecoder.filenameSymbol.asString.dirname +/+ "../config/")
	}

	*new { |configPath, decoderName, satOrder, subOrder, inputFormat, listeningRadius, server|
		^super.newCopyArgs(configPath, decoderName, satOrder, subOrder, inputFormat, listeningRadius, server).init;
	}

	init {



		configPath = this.stringToPathName(configPath);

		patchConfig = configPath.fullPath.load;

		decoderConfig = PathName(configPath.parentPath +/+ patchConfig.decoderFolder +/+ decoderName ++ ".scd").fullPath.load;

		maxOrder = decoderConfig.maxOrder; // default to nil for discrete

		satOrder = satOrder ?? { maxOrder };
		subOrder = subOrder ?? { 1 };
		inputFormat = inputFormat ?? { 'atk' };
		server = server ?? { Server.default };

		// limit fuma to 3rd order??? Why is this?
		((inputFormat == 'fuma') and: (satOrder > 3)).if({ satOrder = 3 });

		numSatChans = decoderConfig.numSatChans { 8 };
		numSubChans = decoderConfig.numSubChans ?? { 0 };

		outbusNums = decoderConfig.outbusNums ?? { (0..numSatChans + numSubChans-1) };
		satOuts = outbusNums[0..numSatChans - 1];
		subOuts = outbusNums[numSatChans..];

		orientation = decoderConfig.orientation ?? { 'flat' };

		discreteType = decoderConfig.discreteType; // nil as default
		encodeType = decoderConfig.encodeType ?? { false }; // false as default for no transcoding



		// check is order is greater than max allowable order for this decoder
		maxOrder.notNil.if({
			((satOrder > maxOrder) or: encodeType).if({ satOrder = maxOrder })
		});

		satDirections = patchConfig.directions[satOuts] ?? { numSatChans.collect{ |inc| inc/numSatChans * 2pi } };
		subDirections = patchConfig.directions[subOuts] ?? { numSubChans.collect{ |inc| inc/numSubChans * 2pi } }; // could be empty array with sub size of zero check if this works

		xover_freq = patchConfig.xover_freq; // default to nil, no crossover (maybe crossover in hardware?)

		(satDirections.size != numSatChans).if({ Error("Given number of satellites: %, does not equal given number of directions: %".format(numSatChans, satDirections.size)).throw });
		(subDirections.size != numSubChans).if({ Error("Given number of subs: %, does not equal given number of directions: %".format(numSubChans, subDirections.size)).throw });

		numBChannels = HoaOrder.new(satOrder).size;
		numInputChannels = numBChannels;

		spDists = patchConfig.spkrDists ?? { (numSatChans + numSubChans).collect{ AtkHoa.refRadius } };

		spGains = patchConfig.spkrGains ?? { (numSatChans + numSubChans).collect{ 0.0 } };

		spDels = patchConfig.spkrDels ?? {
			(spDists.maxItem - spDists) * AtkHoa.speedOfSound.reciprocal;
		};

		decoderType = decoderConfig.decoderType ?? { 'modeMatch' };

		beamShape = decoderConfig.beamShape ?? { 'basic' };

		match = decoderConfig.match ?? { 'energy' };

		this.prLoadDecoder;

		// check if we have different distanced speakers, if not no need to run the delays
		spDels.do{ |del|
			(del != spDels[0]).if({
				useDelays = true;
			})
		};

		decoderConfig.focl.notNil.if({
			useKernels = true;
			multiBandFocl = HoaMultiBandFocl.new(decoderConfig.focl, satOrder, subOrder, numSatChans, numSubChans, listeningRadius, server);
		});

		decoderSynth = this.synthDef

	}

	designModeMatcher { |beamShape = 'basic', match = 'energy'|
		matrix_dec_sat = HoaMatrixDecoder.newModeMatch(satDirections, beamShape, match, satOrder)
	}

	designPanto { |beamShape = 'basic', match = 'energy'|
		matrix_dec_sat = HoaMatrixDecoder.newPanto(numSatChans, orientation, beamShape, match, satOrder)
	}

	designADTAllRad { |filename = 'ADT', beamShape = 'energy', match = 'amp', imagSpeakers, overwrite = false, verbose = false|
		var adt;
		// will write to ~/Library/Application Support/ATK/extensions/matrices/HOA(satOrder)/decoders/
		adt = ADT.new(
			directions: satDirections,
			match: match,
			order: satOrder,
			filename: filename
		);
		adt.filePath("allrad", beamShape).isFile.if({
			overwrite.if({
				adt.allrad(imagSpeakers, 'octave', verbose: verbose)
			}, {
				"File Exists. Use overwrite param to calculate matrix again".warn
			})
		}, {
			adt.allrad(imagSpeakers, 'octave', verbose: verbose)
		});

		this.designFromMatrix(filename, beamShape, match);

	}

	designFromMatrix { |filename = 'ADT', beamShape = 'energy', match = 'amp', pathToMatrix|
		pathToMatrix.notNil.if({
			matrix_dec_sat = HoaMatrixDecoder.newFromFile(pathToMatrix, false, satOrder)
		}, {
			matrix_dec_sat = HoaMatrixDecoder.newFromFile(filename ++ "order-" ++ satOrder ++ "-allrad-beam-" ++ beamShape ++ "-match-" ++ match ++ ".yml", order: satOrder);
		});
	}

	designSubModeMatcher { |beamShape = 'basic', match = 'energy'|
		matrix_dec_sub = HoaMatrixDecoder.newModeMatch(subDirections, beamShape, match, subOrder)
	}

	designSubDirections { |beamShape = 'basic', match = 'energy'|
		matrix_dec_sub = HoaMatrixDecoder.newDirections(subDirections, beamShape, match, subOrder)
	}

	synthDef {

		^CtkSynthDef( decoderName.asSymbol, {
			|out_busnum=0, in_busnum, xover_freq = 120, fadeTime=0.2, satGain = 0, subGain=0, gate=1, amp=1, rotate=0.0, shelfFreq=0.0|
			var in, env, out;

			env = EnvGen.kr(Env([0, 1, 0], [fadeTime, fadeTime], 'sin', 1), gate, doneAction:2);

			in = In.ar(in_busnum, numInputChannels); // Hoa or Discrete

			out = this.ar(in, xover_freq, satGain, subGain, rotate) * env * amp;

			outbusNums.do({ | spkdex, i |
				Out.ar(
					out_busnum + spkdex, // remap decoder channels to rig channels
					// apply delay if necessary
					useDelays.if({
						DelayN.ar(out[i] * spGains[spkdex].dbamp, spDels.maxItem, spDels[spkdex])
					}, {
						out[i] * spGains[spkdex].dbamp
					})
				)
			})
		})

	}

	// discrete pass through
	// seprate sats and subs, for crossover
	arDiscretePass { |in|
		^[in[0..numSatChans - 1], in[numSatChans..]]
	}

	// Bformat decoder
	arBFormat { |in, rotate, encode = false, formatEncode = false|
		var sats, subs, outs;
		// encode to BFormat if necessary
		encode.if({ in = this.prSatEncode(in, spDists[satOuts].mean) });

		// encode from FuMa or AmbiX
		formatEncode.if({ in = this.prFormatEncode(in) });

		// apply rotation
		in = HoaRotate.ar(in, rotate, satOrder);

		// apply FIR kernels for shelving/focalization??
		outs = useKernels.if({
			multiBandFocl.ar(in)
		}, {
			[in, in[subOrder.asHoaOrder.indices]]
		});

		sats = outs[0];
		subs = outs[1];

		// decode sats and subs
		sats = this.prBFormatSatDecode(sats);
		subs = this.prBFormatSubDecode(subs);
		^[sats, subs]

	}

	// discrete routing, use BFormat to matrix subs
	arSatPassSubEncode { |in|
		var subB, subOut;
		subB = this.prSubEncode(in);
		subOut = (numSubChans > 1).if({
			this.prBFormatSubDecode(subB)
		}, {
			subB[0]
		});
		^[in, subOut]
	}

	ar { |in, xover = 120, satGain = 0, subGain = 0, rotate = 0.0|
		var outs;

		outs = case
		{ decoderType != 'discrete' } {
			// BFormat input and output
			// check input format type for possible encoding to 'atk'
			this.arBFormat(in, rotate, false, ((inputFormat == 'fuma') or: (inputFormat == 'ambix')))
		}
		{ encodeType } {
			// discrete routing
			// use bformat to matrix the output only
			this.arBFormat(in, rotate, true, false)
		}
		{ discreteType != 'all' } {
			// discrete passthrough
			// use bformat to matrix subs only
			this.arSatPassSubEncode(in)
		}
		{
			// passthrough everything
			this.arDiscretePass(in)
		};

		xover_freq.notNil.if({
			// apply crossover to sats and subs
			outs = this.prXOver(outs[0], outs[1], xover)
		});

		// apply sat and sub gains
		^(outs[0] * satGain.dbamp) ++ (outs[1] * subGain.dbamp)

	}

	loadKernels {
		useKernels.if({
			multiBandFocl.kernelBuffers.do{ |buffer| buffer.load };
			multiBandFocl.buffersActive = true
		})
	}

	addKernelsTo { |score|
		useKernels.if({
			multiBandFocl.kernelBuffers.do{ |buffer| buffer.addTo(score) }
		})
	}

	stringToPathName { |path, relativePath = (PathName(File.realpath(HoaDecoder.filenameSymbol)).pathOnly)|
		PathName(path).isAbsolutePath.if({//it's absolute path or standard path
			^PathName.new(path);
		}, { // its relative path
			^PathName.new(relativePath ++ path)
		})

	}

	prFormatEncode { |in|
		^HoaEncodeMatrix.ar(
			HoaNFDist.ar(in, satOrder), // convert refRadius to AtkHoa.refRadius
			HoaMatrixEncoder.newFormat(inputFormat, satOrder) // convert input format to 'atk'
		)
	}

	prSatEncode { |in, radius = (AtkHoa.refRadius)|
		// for discrete, sometimes we transcode
		^HoaNFCtrl.ar(HoaEncodeMatrix.ar(in, matrix_enc_sat), radius, AtkHoa.refRadius, satOrder)
	}

	prSubEncode { |in|
		// for discrete, sometimes we transcode
		^HoaEncodeMatrix.ar(in, matrix_enc_sub)
	}

	// main decoder
	prBFormatSatDecode { |satIn|
		^satOuts.collect({ |spkrdex, matrixRow|
			(
				HoaNFCtrl.ar(
					satIn,
					AtkHoa.refRadius,
					spDists.at(spkrdex),
					satOrder
				) * matrix_dec_sat.matrix.getRow(matrixRow)
			).sum
		})
	}

	// sub decoder
	// if only one sub, scale and return W channel
	prBFormatSubDecode { |subIn|
		^(numSubChans > 1).if({
			subOuts.collect({ |subdex, matrixRow|
				(
					HoaNFCtrl.ar(
						subIn,
						AtkHoa.refRadius,
						spDists.at(subdex),
						subOrder
					) * matrix_dec_sub.matrix.getRow(matrixRow)
				).sum
			})
		}, {
			subIn[0]
		})
	}

	prXOver { |sats, subs, xover|
		^[HPF.ar(HPF.ar(sats, xover), xover), LPF.ar(LPF.ar(subs, xover), xover)]
	}

	prLoadDecoder {
		// design the sat matrix decoder
		matrix_dec_sat = case
		{ decoderType == 'matrix' } {
			matrixFilePath = decoderConfig.matrixFilePath;
			matrixFilePath.isNil.if({ Error("Please supply a matrix file path").throw });
			// if full path is give, load that, otherwise assume name is from ADT??
			PathName(matrixFilePath).isAbsolutePath.if({
				HoaMatrixDecoder.newFromFile(matrixFilePath, false, satOrder)
			}, {
				HoaMatrixDecoder.newFromFile(matrixFilePath ++ "-order-" ++ satOrder ++ "-allrad-beam-" ++ beamShape ++ "-match-" ++ match ++ ".yml", order: satOrder);
			})
		}
		{ decoderType == 'modeMatch' } {
			HoaMatrixDecoder.newModeMatch(satDirections, beamShape, match, satOrder)
		}
		{ decoderType == 'panto' } {
			HoaMatrixDecoder.newDirections(satDirections, beamShape, match, satOrder)
		}
		{ decoderType == 'discrete' } {
			(discreteType != 'all').if({
				var directions = discreteType.switch
				{ 'quad' } { [0.25pi, -0.25pi, 0.75pi, -0.75pi] }
				{ 'hex_flat' } { [pi/6, -pi/6, 0.5pi, -0.5pi, 5pi/6, -5pi/6] }
				{ 'hex_vertex' } { [0, pi/3, -pi/3, 2pi/3, -2pi/3, -pi] }
				{ 'oct_flat' } { [pi/8, -pi/8, 3pi/8, -3pi/8, 5pi/8, -5pi/8, 7pi/8, -7pi/8] }
				{ 'oct_vertex' } { [0, pi/4, -pi/4, 0.5pi, -0.5pi, 3pi/4, -3pi/4, -pi] }
				{ Error("Encoder Type:" + discreteType + "not supported").throw };
				numInputChannels = directions.size;
				matrix_enc_sat = HoaMatrixEncoder.newDirections(directions, beamShape, match, satOrder);
				matrix_enc_sub = HoaMatrixEncoder.newDirections(matrix_enc_sat.directions, beamShape, match, subOrder);
				inputFormat = 'atk'; // force input format in case of mistake
				HoaMatrixDecoder.newDirections(satDirections, beamShape, match, satOrder)
			}, {
				numInputChannels = outbusNums.size;
				useKernels = false; // force no kernel is passthrough discrete
				nil
			})
		}
		{ Error("Please choose from 'matrix', 'modeMatch', 'panto', or 'discrete' decoder types").throw };

		// design the sub matrix, if more than one channel use direcitons, otherwise use nil for W channel
		matrix_dec_sub = case
		{ (numSubChans > 1) and: (discreteType != 'all') } {
			HoaMatrixDecoder.newDirections(subDirections, beamShape, match, subOrder)
		}
		{ nil }
	}

}


